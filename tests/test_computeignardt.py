

import unittest
import computeignardt

class TestComputeignardt(unittest.TestCase):

    compute = computeignardt.ComputeCalls()

    def test_count(self):
        self.compute.power(2, 3)
        self.compute.power(2, 4)
        self.compute.log(8, 2)
        self.assertEqual(self.compute.calls(), 3)
        print("Hemos llamado a las funciones", self.compute.calls(), "veces")

if __name__ == '__main__':
    unittest.main()