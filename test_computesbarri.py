
import unittest
import computesbarri

class Testcomputesbarri(unittest.Testcase):

    compute = computesbarri.ComputeCalls()

    def test_count(self):
        self.compute.power(2, 3)
        self.compute.power(2, 4)
        self.compute.log(8, 3)
        self.assertEqual(self.compute.calls(), 3)
        print ("Se ha llamado", self.compute.calls(), "veces a las funciones")

if __name__ == '__main__':
    unittest.main()

